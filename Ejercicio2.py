# !/usr/bin/env python3
# -*- coding: utf-8 -*-


import random


# Procedieminto para sacar el numero menor de la matriz
def menor(matriz):
    menor = matriz[0][0]
    for i in range(15):
        for j in range(12):
            if menor > matriz[i][j]:
                menor = matriz[i][j]
    print("El numero menos es:", menor)


# Procedimiento para sumar las primeras 5 primeras columnas
def suma(matriz):
    suma = 0
    for i in range(5):
        for j in range(12):
            suma += matriz[j][i]
    print("La suma de las 5 primeras columnas es:", suma)


# Procedimeinto para sacar el total de numero negativos de la matriz
def negativos(matriz):
    neg = 0
    for i in range (15):
        for j in range(12):
            if matriz[i][j] < 0:
                neg += 1
    print("Total de numero negativos es:", neg)


# Procedieminto para imprimir la matriz
def imprimir(matriz):
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j], end=' ')
        print()


# Procedimiento para realizar la matriz
def matriz():
    matriz = []
    print("\n")
    for i in range(15):
        matriz.append([])
        for j in range(12):
            matriz[i].append(random.randint(-10, 10))
    imprimir(matriz)
    menor(matriz)
    suma(matriz)
    negativos(matriz)


# Función principal o main
if __name__ == "__main__":
    matriz()
