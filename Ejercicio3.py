# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import random


# Procedieminto para imprimir la matriz
def imprimir(matriz):
    print("Notas:")
    for i in range(len(matriz)):
        for j in range(len(matriz[i])):
            print(matriz[i][j], end=' ')
        print()


# Procedimiento para sacar el promedio
def promedio(matriz):
    aprobaron = 0
    reprobaron = 0
    # Se recorre la matriz
    for i in range(31):
        prom = 0
        for j in range(1, 5):
            prom += matriz[i][j]
        prom = prom / 4
    # Condiciones para ver si el alumno aprueba o no
        if prom >= 4:
            reprobaron += 1
            print("El estudiante", i + 1, "aprueba con un:", prom)
        else:
            aprobaron += 1
            print("El estudiante", i + 1, "reprueba con un:", prom)

    # Imprimimos el total de alumnos que reprobaron y aprobaron
    print("El total de aprobados son:", aprobaron)
    print("El total de reprobados son:", reprobaron)


# Procedimiento para realizar la matriz con notas aleatorias
def matriz():
    matriz = []
    # Matriz con 31 estudientes y sus notas
    for i in range(31):
        matriz.append([i + 1])
        for j in range(4):
            matriz[i].append(round(random.uniform(1, 7), 1))
    imprimir(matriz)
    promedio(matriz)


# Función principal o main
if __name__ == "__main__":
    matriz()
