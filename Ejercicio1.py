# !/usr/bin/env python3
# -*- coding: utf-8 -*-


# Procedimiento para odenar la lista como se pide
def ordenar(tupla, lista):
    # Para rellenar la lista con la tupla
    for i in range(len(tupla)):
        lista.append([tupla])
    print(tupla)

    # Para que el ultimo elemento se convierta en el primero
    for i in range(len(tupla)):
        lista[i - 1] = tupla[i]
    print(lista)


# Función principal o main
if __name__ == "__main__":
    # Tupla definida
    tupla = (1, 2, 3, 4, 5, 6, 7, 8)
    # Lista vacia
    lista = []
    ordenar(tupla, lista)
